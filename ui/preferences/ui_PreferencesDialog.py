# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'X:\pkgPower\ui\preferences\PreferencesDialog.ui'
#
# Created: Thu Jun 21 20:24:37 2012
#      by: PyQt4 UI code generator 4.8.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_PReferencesDialog(object):
    def setupUi(self, PReferencesDialog):
        PReferencesDialog.setObjectName(_fromUtf8("PReferencesDialog"))
        PReferencesDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        PReferencesDialog.resize(484, 137)
        PReferencesDialog.setWindowTitle(QtGui.QApplication.translate("PReferencesDialog", "Preferences", None, QtGui.QApplication.UnicodeUTF8))
        PReferencesDialog.setModal(True)
        self.verticalLayout = QtGui.QVBoxLayout(PReferencesDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.label = QtGui.QLabel(PReferencesDialog)
        self.label.setText(QtGui.QApplication.translate("PReferencesDialog", "text editor", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setObjectName(_fromUtf8("label"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label)
        self.textEditorChooser = FileChooserLineEdit(PReferencesDialog)
        self.textEditorChooser.setToolTip(QtGui.QApplication.translate("PReferencesDialog", "Specify the path to your favourite text editor here.", None, QtGui.QApplication.UnicodeUTF8))
        self.textEditorChooser.setObjectName(_fromUtf8("textEditorChooser"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.textEditorChooser)
        self.label_2 = QtGui.QLabel(PReferencesDialog)
        self.label_2.setText(QtGui.QApplication.translate("PReferencesDialog", "vnc viewer", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_2)
        self.vncChooser = FileChooserLineEdit(PReferencesDialog)
        self.vncChooser.setToolTip(QtGui.QApplication.translate("PReferencesDialog", "Specify the path to your VNC viewer here.", None, QtGui.QApplication.UnicodeUTF8))
        self.vncChooser.setObjectName(_fromUtf8("vncChooser"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.vncChooser)
        self.label_3 = QtGui.QLabel(PReferencesDialog)
        self.label_3.setText(QtGui.QApplication.translate("PReferencesDialog", "vnc regex", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_3)
        self.vncRegexLineEdit = QtGui.QLineEdit(PReferencesDialog)
        self.vncRegexLineEdit.setToolTip(QtGui.QApplication.translate("PReferencesDialog", "pkgPower will use this string to determine the hostname from the filename. It expects regular python regex syntax with a named group calles \"hostname\".", None, QtGui.QApplication.UnicodeUTF8))
        self.vncRegexLineEdit.setObjectName(_fromUtf8("vncRegexLineEdit"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.vncRegexLineEdit)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtGui.QDialogButtonBox(PReferencesDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(PReferencesDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), PReferencesDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), PReferencesDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(PReferencesDialog)

    def retranslateUi(self, PReferencesDialog):
        pass

from ui.widgets.FileChooserLineEdit import FileChooserLineEdit
