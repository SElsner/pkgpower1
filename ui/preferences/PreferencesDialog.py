from PyQt4.QtGui import QDialog
from PyQt4.QtCore import QSettings

from ui_PreferencesDialog import Ui_PReferencesDialog

class PreferencesDialog(QDialog, Ui_PReferencesDialog):

    """Dialog to edit the apps preferences"""

    def __init__(self, parent = None):
        QDialog.__init__(self, parent)
        Ui_PReferencesDialog.__init__(self)
        self.setupUi(self)

        self.loadPreferences()

    def savePreferences(self):
        """
        Save all preferences.
        """
        settings = QSettings()
        settings.setValue("texteditor", self.textEditorChooser.currentFilePath())
        settings.setValue("vncviewer", self.vncChooser.currentFilePath())
        settings.setValue("vncregex", self.vncRegexLineEdit.text())

    def loadPreferences(self):
        """
        Load all preferences.
        """
        settings = QSettings()
        self.textEditorChooser.setCurrentFilePath(settings.value("texteditor", ""))
        self.vncChooser.setCurrentFilePath(settings.value("vncviewer", ""))
        self.vncRegexLineEdit.setText(settings.value("vncregex", r"wpkg-(?P<hostname>.+).log"))

    def accept(self, *args, **kwargs):
        """
        Save all preferences when user clicks OK.
        """
        self.savePreferences()
        return QDialog.accept(self, *args, **kwargs)

def getTextEditorPath():
    '''
    Returns the text editor path specified in the preferences.
    '''
    settings = QSettings()
    return settings.value("texteditor", "")

def getVncViewerPath():
    '''
    Returns the vnc viewer path specified in the preferences.
    '''
    settings = QSettings()
    return settings.value("vncviewer", "")

def getVncRegex():
    '''
    Returns the regex used to extract the hostname from the log file name.
    '''
    settings = QSettings()
    return settings.value("vncregex", r"wpkg-(?P<hostname>.+).log")
