import os

from PyQt4.QtGui import QComboBox, QFileDialog
from PyQt4.QtCore import QSettings


class FolderChooserComboBox(QComboBox):

    """
    ComboBox which has an additional default entry to browse 
    the filesystem and select a folder. Remembers the previously
    selected paths.
    """

    def __init__(self, parent = None):
        QComboBox.__init__(self, parent)

        self.currentIndexChanged[str].connect(self.locationChanged)

    def setCurrentIndex(self, index):
        """
        Override QComboBox default setCurrentIndex for the special case of
        the Browse entry selected.
        """

        if self.itemText(index) == 'Browse...':
            index = -1
        QComboBox.setCurrentIndex(self, index)

    def loadRecentLocations(self):
        """
        Loads the previous locations specified in this widget.
        """

        self.blockSignals(True)
        settings = QSettings()
        values = settings.value(self.objectName(), [])
        if values:
            self.addItems(values)
        self.addItem("Browse...")
        if len(values) == 0:
            self.setCurrentIndex(-1)
        else:
            self.setCurrentIndex(0)
        self.blockSignals(False)

    def saveRecentLocations(self):
        """
        Saves the locations used in this widget.
        """

        settings = QSettings()
        values = settings.value(self.objectName(), [])
        if self.currentText() not in values:
            values.append(self.currentText())
        for i in range(self.count()):
            t = self.itemText(i)
            if len(t) and (t not in ['Browse...'] + values):
                values.append(t)
        settings.setValue(self.objectName(), values)

    def locationChanged(self, text):
        """
        Called when the current index changes. Open the folder selection
        dialog if the Browse entry was selected.
        """

        self.blockSignals(True)
        if text == "Browse...":
            selDir = QFileDialog.getExistingDirectory()
            if selDir:
                selDir = os.path.normpath(selDir)
                self.insertItem(0, selDir)
                self.setCurrentIndex(0)
            else:
                self.setCurrentIndex(0)
        self.blockSignals(False)
