from PyQt4.QtGui import QWidget, QFileDialog, QLineEdit, QToolButton, QHBoxLayout
import os



class FileChooserLineEdit(QWidget):
    '''
    Generic file chooser widget with a LineEdit and a Button to select
    a file from the filesystem.
    '''

    def __init__(self, parent = None):
        QWidget.__init__(self, parent)

        self.pathLineEdit = QLineEdit()
        self.openToolButton = QToolButton()
        self.openToolButton.setText("...")
        self.openToolButton.clicked.connect(self.openFileChooser)

        horizontalLayout = QHBoxLayout()
        horizontalLayout.addWidget(self.pathLineEdit)
        horizontalLayout.addWidget(self.openToolButton)
        self.setLayout(horizontalLayout)

        horizontalLayout.setContentsMargins(0, 0, 0, 0)

    def openFileChooser(self):
        """
        Called when the user clicks the file open button.
        Shows a FileDialog and sets the LineEdit to the
        path returned.
        """
        filepath = QFileDialog.getOpenFileName(self)
        if os.path.exists(filepath):
            self.pathLineEdit.setText(filepath)

    def currentFilePath(self):
        """
        Returns the current path in the LineEdit.
        """
        return self.pathLineEdit.text()

    def setCurrentFilePath(self, path):
        """
        Sets the current file path line edit to path.
        """

        self.pathLineEdit.setText(path)
