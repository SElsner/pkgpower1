from PyQt4.QtCore import QAbstractTableModel, Qt
from PyQt4.QtGui import  QSortFilterProxyModel

from model.log.Log import Log


LogDetailsTableColumns = [("", 23), ("date", 125), ("message", 100) ]

class LogDetailsTableProxyModel(QSortFilterProxyModel):
    '''
    Proxy model for filtering of the log details table. Implements standard
    behaviour of a Proxy model with row filtering. Sorting is not enabled.
    '''

    def __init__(self):
        QSortFilterProxyModel.__init__(self)
        self.sourceModel_ = LogDetailsTableModel()
        self.setSourceModel(self.sourceModel_)

        self.logLevelFilter = ''
        self.logMessageFilter = ''

        self.logDateStart = None
        self.logDateEnd = None

    def setLog(self, log):
        '''
        @param log: Set the log for which we want to set the details. 
        '''
        self.sourceModel().setLog(log)

    def filterAcceptsRow(self, sourceRow, sourceParent):
        '''
        Reimplemented from QSortFilterProxyModel. Decides, if a row should be accepted if filtered.
        '''
        logLine = self.sourceModel_.getLine(sourceRow)
        if self.logLevelFilter:
            if logLine.level != self.logLevelFilter:
                return False
        if self.logMessageFilter:
            if logLine.message.find(self.logMessageFilter) == -1:
                return False
        if self.logDateStart and self.logDateEnd:
            if not ((logLine.datetime <= self.logDateEnd) and (logLine.datetime >= self.logDateStart)):
                return False
        return True

    def setLogLevelFilter(self, value):
        '''
        Sets the log level filter member to value and invalidates the current filtering.
        @param value: value of the filter, normally this would be a sting: DEBUG, INFO, WARNING or ERROR
        '''
        self.logLevelFilter = value
        self.invalidateFilter()

    def setLogMessageFilter(self, value):
        '''
        Sets the log message filter member to value and invalidates the current filtering.
        @param value: message part to filter
        '''
        self.logMessageFilter = value
        self.invalidateFilter()

    def setLogDateEndChanged(self, datetime):
        '''
        Sets the log end datetime filter member to given datetime and invalidates the current filtering.
        @param datetime: end datetime of filter
        '''
        self.logDateEnd = datetime.toPyDateTime()
        self.invalidateFilter()

    def setLogDateStartChanged(self, datetime):
        '''
        Sets the log start datetime filter member to given datetime and invalidates the current filtering.
        @param datetime: start datetime of filter
        '''
        self.logDateStart = datetime.toPyDateTime()
        self.invalidateFilter()


class LogDetailsTableModel(QAbstractTableModel):
    '''
    Base model for log details table. Holds data of the current log.
    '''

    def __init__(self):
        QAbstractTableModel.__init__(self)
        self.log = Log()

    def setLog(self, log):
        """
        Notify all components of a change in the model and set set the 
        current log to the given log given in log.
        """

        self.modelAboutToBeReset.emit()
        self.log = log
        self.modelReset.emit()

    def rowCount(self, idx):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        return self.log.numLines()

    def columnCount(self, idx):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        return len(LogDetailsTableColumns)

    def getLine(self, lineNumber):
        '''
        Returns the LogLine at the given line number.
        @param lineNumber: line number of the LogLine in the currently displayed log
        '''
        return self.log.getLine(lineNumber)

    def data(self, index, role):
        '''
        Reimplemented from QAbstractTableModel. Responsible for displaying the correct data for the given index and role.
        '''

        if not index.isValid():
            return None

        logLine = self.log.getLine(index.row())
        columnName = LogDetailsTableColumns[index.column()][0]
        if role == Qt.DecorationRole:
            if columnName == '':
                return logLine.icon()
        elif role == Qt.DisplayRole:
            if columnName == 'date':
                return str(logLine.datetime)
            elif columnName == 'message':
                return logLine.message
        elif role == Qt.UserRole:
            if columnName == '':
                return logLine.level
            elif columnName == 'date':
                return logLine.datetime
        elif role == Qt.EditRole:
            if columnName == 'message':
                return logLine.message
        elif role == Qt.ToolTipRole:
            if columnName == '':
                return logLine.level
            elif columnName == 'date':
                return "date and time of this log line"
            elif columnName == 'message':
                return "actual log entry message"
        return None

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return LogDetailsTableColumns[section][0]
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return section
        else:
            return None

    def flags(self, index):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        if index.column() == 2:
            return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable
        return QAbstractTableModel.flags(self, index)
