import os
import datetime

from PyQt4.QtGui import QMainWindow, QDesktopServices, QMessageBox, QAbstractItemView, QApplication, qApp, QHeaderView
from PyQt4.QtCore import QUrl, QDir, QModelIndex, QSettings, QSize, QPoint, Qt

from ui.mainwindow.ui_MainWindow import Ui_MainWindow
from ui.resources.iconcache import appIcons
from ui.preferences.PreferencesDialog import PreferencesDialog
from ui.mainwindow.logtable.LogsTableModel import LogTableProxyModel, \
    LogsTreeColumns
from ui.mainwindow.logdetails.LogDetailsTableModel import LogDetailsTableProxyModel, \
    LogDetailsTableColumns

class MainWindow(QMainWindow, Ui_MainWindow):
    '''
    Main window of the application.
    '''

    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        #setup log viewer model and view
        self.logModel = LogTableProxyModel()
        self.logListTableView.setModel(self.logModel)
        self.logListTableView.clicked.connect(self.selectedLogChanged)
        self.logListTableView.selectionModel().currentChanged.connect(self.selectedLogChanged)
        self.refreshDirectoryToolButton.clicked.connect(self.refreshLogs)
        for i, column in enumerate(LogsTreeColumns):
            self.logListTableView.setColumnWidth(i, column[1])

        self.logListTableView.horizontalHeader().setResizeMode(1, QHeaderView.Stretch)

        #setup log details model an view
        self.logDetailsModel = LogDetailsTableProxyModel()
        self.logDetailsTableView.setModel(self.logDetailsModel)
        for i, column in enumerate(LogDetailsTableColumns):
            self.logDetailsTableView.setColumnWidth(i, column[1])

        #setup log folder
        self.directoryComboBox.loadRecentLocations()

        #setup log view widgets
        self.refreshDirectoryToolButton.setIcon(appIcons['refresh'])
        self.openToolButton.setIcon(appIcons["folder"])
        self.openToolButton.clicked.connect(self.openLogDirectory)

        #setup log details filter widgets
        self.logLevelComboBox.currentIndexChanged[str].connect(self.logDetailsModel.setLogLevelFilter)
        self.messageLineEdit.textChanged.connect(self.logDetailsModel.setLogMessageFilter)
        self.logDateEndDateTimeEdit.dateTimeChanged.connect(self.logDetailsModel.setLogDateEndChanged)
        self.logDateStartDateTimeEdit.dateTimeChanged.connect(self.logDetailsModel.setLogDateStartChanged)

        #setup menu action
        self.actionPreferences.triggered.connect(self.openPreferencesDialog)

        self.readSettings()



    def openPreferencesDialog(self):
        '''
        Open preferences dialog.
        '''
        dlg = PreferencesDialog(self)
        dlg.show()

    def openLogDirectory(self):
        '''
        With the help of Qt's desktop services open the OS native file browser at the log folder.
        '''
        text = QDir.toNativeSeparators(self.directoryComboBox.currentText())
        if os.path.exists(text):
            QDesktopServices.openUrl(QUrl(text))
        else:
            QMessageBox.critical(self, "Error", "Path does not exist.")

    def selectedLogChanged(self, current, previous = None):
        '''
        Called when the user changes the current log he wants to see the 
        details for. Also 
        @param current: current log ModelIndex
        @param previous: previous log ModelIndex
        '''
        log = self.logModel.getLog(current) #get the log clicked on
        self.logDetailsModel.setLog(log) #notify the model
        start, end = log.getTimeframe()
        # set the datetime filter widgets, add/subtract 5 minutes, so we include all log lines even
        # if the clocks and times in the log are off 
        self.logDateEndDateTimeEdit.setDateTime(end + datetime.timedelta(minutes = 5))
        self.logDateStartDateTimeEdit.setDateTime(start - datetime.timedelta(minutes = 5))
        #check if the user clicked on the error/warning icons and jump to the
        #first error/warning line available
        if current.column() == 0 and (log.hasErrors() or log.hasWarnings()):
            self.logDetailsTableView.scrollTo(self.logDetailsModel.index(log.nextErrorWarningLine(), 0, QModelIndex()), QAbstractItemView.PositionAtCenter)
        else:
            self.logDetailsTableView.scrollToTop()

    def refreshLogs(self):
        '''
        Refresh the logs shown.
        '''
        self.logModel.setLogDirectory(self.directoryComboBox.currentText())

    def closeEvent(self, *args, **kwargs):
        '''
        On exit make sure we remember some of the user data.
        '''
        self.directoryComboBox.saveRecentLocations()
        self.writeSettings()
        return QMainWindow.closeEvent(self, *args, **kwargs)

    def writeSettings(self):
        settings = QSettings()
        settings.setValue("mainwindow/size", self.size())
        settings.setValue("mainwindow/pos", self.pos())
        settings.setValue("mainwindow/logsplitter", self.splitter.saveState())

    def readSettings(self):
        settings = QSettings()
        size = settings.value("mainwindow/size")
        pos = settings.value("mainwindow/pos")
        if size and pos:
            self.resize(size)
            self.move(pos)
        else:
            self.setWindowState(Qt.WindowMaximized)

        splitterState = settings.value("mainwindow/logsplitter")
        if splitterState:
            self.splitter.restoreState(splitterState)


