from PyQt4.QtCore import QAbstractTableModel, Qt
from PyQt4.QtGui import  QSortFilterProxyModel, QBrush, QColor

import datetime

from model.log.LogCache import LogCache

LogsTreeColumns = [("", 23), ("Name", 150), ("modified", 115) , ("age (days)", 60)]

class LogTableProxyModel(QSortFilterProxyModel):
    '''
    Proxy model for sorting of the log table. Takes advantage of the default
    sorting mechanism provided by Qt. There is not filtering in this model.
    '''

    def __init__(self):
        QSortFilterProxyModel.__init__(self)
        self.sourceModel_ = LogsTableModel()
        self.setSourceModel(self.sourceModel_)

    def setLogDirectory(self, dir):
        '''
        Set the log directory we want to be displayed in the view.
        @param dir: full path to directory
        '''
        self.sourceModel_.setLogDirectory(dir)

    def getLog(self, index):
        '''
        Return the log at the given ModelIndex
        '''
        return self.sourceModel_.getLog(self.mapToSource(index))

class LogsTableModel(QAbstractTableModel):
    '''
    Base model for log table. Holds data of all the log in a log cache.
    '''

    def __init__(self):
        QAbstractTableModel.__init__(self)
        self.logCache = LogCache()

    def setLogDirectory(self, dir):
        """
        Notify all components of a change in the model and set the 
        given dir to be used by the cache.
        """

        self.modelAboutToBeReset.emit()
        self.logCache.loadLogs(dir)
        self.modelReset.emit()

    def rowCount(self, idx):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        return self.logCache.numLogs()

    def columnCount(self, idx):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        return len(LogsTreeColumns)

    def getLog(self, index):
        """
        Returns the log at the given ModelIndex
        """
        return self.logCache.getLog(index.row())

    def data(self, index, role):
        '''
        Reimplemented from QAbstractTableModel.  Responsible for displaying the correct data for the given index and role.
        '''
        if not index.isValid():
            return None

        log = self.getLog(index)
        columnName = LogsTreeColumns[index.column()][0]
        if role == Qt.DecorationRole:
            if columnName == '':
                return log.icon()
        elif role == Qt.DisplayRole:
            if columnName == 'Name':
                return log.name
            elif columnName == "modified":
                return str(datetime.datetime.fromtimestamp(log.mtime)).split(".")[0]
            elif columnName == "age (days)":
                return (datetime.datetime.now() - datetime.datetime.fromtimestamp(log.mtime)).days
        elif role == Qt.BackgroundRole:
            if columnName == "age (days)":
                diff = datetime.date.today() - datetime.datetime.fromtimestamp(log.mtime).date()
                return QBrush(QColor(255, max(255 - diff.days * 10, 0), max(255 - diff.days * 10, 0))) if diff.days > 0 else None
        elif role == Qt.ToolTipRole:
            if columnName == '':
                return "Log error level. Click multiple times to jump between errors."
            elif columnName == 'modified':
                return "date and time when this log was last modified"
            elif columnName == "age (days)":
                return "Age of this log."
        return None

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        '''
        Reimplemented from QAbstractTableModel.
        '''
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return LogsTreeColumns[section][0]
        else:
            return None
