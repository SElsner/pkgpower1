import os
from subprocess import Popen
import re

from PyQt4.QtGui import QTableView, QMenu, QAction, QMessageBox
from PyQt4.QtCore import Qt

from ui.preferences.PreferencesDialog import getTextEditorPath, getVncViewerPath, getVncRegex

class LogTableView(QTableView):
    '''
    Customized TableView to handle a context menu.
    '''

    def __init__(self, parent = None):
        QTableView.__init__(self, parent)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showCustomContextMenu)

    def showCustomContextMenu(self, pos):
        '''
        Show a custom context menu at pos. This context menu will
        have options to open the selected log in a text editor and
        vnc to theclient name parsed from the log file name.
        '''

        menu = QMenu(self)
        texteditorAction = QAction("Open in texteditor", menu)
        texteditorAction.triggered.connect(self.openInTextEditor)
        vncAction = QAction("Open VNC Viewer", menu)
        vncAction.triggered.connect(self.openVncViewer)
        menu.addAction(texteditorAction)
        menu.addAction(vncAction)
        menu.exec_(self.mapToGlobal(pos))

    def selectedLog(self):
        """
        Returns the currently selected Log
        """

        selectedIndexes = self.selectedIndexes()
        if len(selectedIndexes):
            return self.model().getLog(selectedIndexes[0])
        return None

    def openInTextEditor(self):
        """
        Opens the currently selected Log in the text editor
        specified in the preferences dialog.
        """

        log = self.selectedLog()
        if log:
            tep = getTextEditorPath()
            if tep and os.path.exists(tep):
                Popen(r'"%s" %s' % (tep, log.path), shell = True)
            else:
                QMessageBox.critical(self, "Error", "No text editor found at %s" % tep)

    def openVncViewer(self):
        """
        Parses the hostname from the log file name via the regex
        string specified in the preferences. Then opens the vnc
        viewer also specified in the preferences with this name as
        first argument.
        """

        log = self.selectedLog()
        if log:
            regex = getVncRegex()
            match = re.search(regex, log.name)
            try:
                hostname = match.group("hostname")
            except:
                QMessageBox.critical(self, "Error", "No valid hostname found in the log's filename using the regex from the VNC viewer regex preferences: %s." % regex)
                return

            viewerPath = getVncViewerPath()
            if viewerPath and os.path.exists(viewerPath):
                Popen(r'"%s" %s' % (viewerPath, hostname), shell = True)
            else:
                QMessageBox.critical(self, "Error", "No VNC viewer found at %s" % viewerPath)


