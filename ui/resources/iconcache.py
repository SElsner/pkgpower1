from PyQt4.QtGui import QIcon

from ui.resources import resources_rc

appIcons = {'WARNING': QIcon(':/icons/warning.png'),
        "ERROR" : QIcon(':/icons/error.png'),
        'INFO' : QIcon(':/icons/info.png'),
        'DEBUG' : QIcon(':/icons/gear.png'),
        'main' : QIcon(':/icons/main.png'),
        'refresh' : QIcon(':/icons/refresh.png'),
        'folder' : QIcon(':/icons/folder.png')}
