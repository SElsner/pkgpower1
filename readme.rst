========
Overview
========

pkgPower is a tool which aims to make certain tasks of every day work with wpkg easy.
Currently pkgPower can parse wpkg log files. The logs are displayed
in a nice interace with features to quickly navigate those logs.

pkgPower runs on Windows and Linux. OSX is untested.

Acknowledgement
===============

pkgPower has partly been developed at `risefx`_ Berlin.

Features
========

The Log Viewer module has the following features:

  * Read and process logs from one central folder
  * Provide a quick overview of the state of the log files
  * Display logs in a sortable table with log error level, name and modification date
  * Color log files by age of file
  * Quick jump to error/ warning lines
  * Open logs in favourite text editor
  * Open VNC viewer with hostname parsed from log name
  * Browse to log folder
  * Live filter and search in log file, by timeframe, error level and message

Dependencies
============

pkgPower uses `Python`_ 2.6. 2.5 and 2.7 are untested, but should work. You will also 
need a recent version of `PyQt4`_.

Installation
============

Checkout the source here at bitbucket, install Python and PyQt4 and run main.py

Contributing
============

pkgPower is an open source project managed using Mercurial and hosted here at bitbucket.
Contributing is as easy as forking the project and committing back your enhancements.

Please note the following guidelines for contributing:

  * Contributed code must be written in the existing style. This is
    as simple as following `PEP 8`_.
  * Contributions must be available on a separately named branch
    based on the latest version of the main branch.

.. GENERAL LINKS

.. _`PEP 8`: http://www.python.org/dev/peps/pep-0008/
.. _`risefx`: http://www.risefx.com

.. THIRD PARTY LIBS

.. _`Python`: http://www.python.org
.. _`PyQt4`: http://www.riverbankcomputing.co.uk/software/pyqt/download
