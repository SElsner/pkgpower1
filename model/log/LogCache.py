import os

from PyQt4.QtGui import QProgressDialog

from model.log.Log import Log

class LogCache(object):

    """Class to hold all Log instances.
    For the moment we assume that all logs are in one directory,
    subdirectories are not yet supported."""

    def __init__(self, path = None):

        self.path = path # full path to log directory
        self.logs = [] #list for all logs

        self.progressDialog = None #progress dialog for load progress indication

        self.loadLogs(path)



    def loadLogs(self, path):
        '''
        Processes all .log files in path
        @param path: string parameter pointing to a path with log files
        '''
#        TODO: does not clean logs, which were deleted on the filesystem
        if path and os.path.exists(path):
            #if path changed, reset
            if path != self.path:
                self.logs = []
                self.path = path

            #get all files in log folder
            filenames = sorted(os.listdir(self.path))
            numFiles = len(filenames)

            self.progressDialog = QProgressDialog('Loading logs...', 'Cancel', 0, numFiles)
            self.progressDialog.setWindowTitle('Processing...')
            self.progressDialog.show()

            #process all files
            for i, name in enumerate(filenames):
                self.progressDialog.setValue(i)
                if self.progressDialog.wasCanceled():
                    return
                if name.endswith('.log'):
                    l = Log(self.path, name)
                    try:
                        li = self.logs.index(l)
                        self.logs[li].process()
                    except:
                        l.process()
                        self.logs.append(l)
            self.progressDialog.setValue(numFiles)

    def numLogs(self):
        '''
        Returns the number of logs in the cache.
        '''
        return len(self.logs)

    def getLog(self, rowNumber):
        '''
        Returns the log at rowNumber
        @param rowNumber: number of the row to return
        '''
        return self.logs[rowNumber]


