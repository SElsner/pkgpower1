
import datetime
import re

from ui.resources.iconcache import appIcons

class LogLine(object):
    """Represents a line in the log."""

    regex = re.compile(r"(.+?), +(.+?) +: +(.+)")

    def __init__(self, line = ""):

        self.datetime = None #datetime object of the line
        self.level = None #error level of the line 
        self.message = None #message of the line

        self.rawLine = line # full line

        self.parseLine()

    def setRawLine(self, line):
        '''
        Set the full raw line to line. Call parseLine afterwards to update
        the instance's members.
        @param line: new line value
        '''
        self.rawLine = line

    def parseLine(self):
        '''
        Parse the line finding values for the datetime, level and message members.
        '''
        m = LogLine.regex.match(self.rawLine)
        if m:
            self.datetime = datetime.datetime.strptime(m.group(1), "%Y-%m-%d %H:%M:%S")
            self.level = m.group(2)
            self.message = m.group(3)
        return m

    def icon(self):
        '''
        Return the appropriate icon according to the line level.
        '''

        try:
            return appIcons[self.level]
        except:
            return None

    def __repr__(self):
        return "LogLine(%s, %s, %s)" % (self.datetime, self.level, self.message)
