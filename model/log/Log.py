
import os

from ui.resources.iconcache import appIcons
from model.log.LogLine import LogLine


class Log(object):

    """Represents a log on the filesystem. Right now we assume that each log is
    uniquely identifiable by the full log path."""

    def __init__(self, dir = '', name = ''):
        '''
        @param dir: directory path of log file
        @param name: log file name
        '''

        self.dir = dir # directory whare log resides in
        self.name = name # name of log file
        self.path = os.path.join(dir, name) #full path of log file
        self.mtime = 0 # modification time of log file

        self.file = None #holds file object of log

        self.logLines = [] #holds all lines of the log

        self.errorLines = []
        self.warningLines = []
        self.debugLines = []
        self.infoLines = []

        self.currentErrorWarningIndex = 0

    def hasErrors(self):
        '''
        Returns True if this log has errors
        '''

        return True if len(self.errorLines) else False

    def hasWarnings(self):
        '''
        Returns True if this log has warnings.
        '''

        return True if len(self.warningLines) else False

    def nextErrorWarningLine(self):
        '''
        Iterates over the error and warning lines found and returns the next line.
        If the end of the error/warning lines is reached, start from the beginning.
        If there are not such lines return the first line.
        '''

        importantLines = self.errorLines + self.warningLines
        try:
            value = importantLines[self.currentErrorWarningIndex]
            self.currentErrorWarningIndex = +1
            return value
        except:
            try:
                value = importantLines[0]
                self.currentErrorWarningLine = 1
                return value
            except:
                return 0

    def __cmp__(self, other):
        '''
        Compares two log files by their paths, so a path mus uniquely identify a Log instance.
        @param other: the log file to compare with
        '''
        return cmp(self.path, other.path)

    def getTimeframe(self):
        '''
        Returns the timeframe of a log as a tuple: (first_log_entry, last_log_entry)
        '''

        if len(self.logLines):
            return (self.logLines[0].datetime, self.logLines[-1].datetime)
        return (self.mtime, self.mtime)

    def getLine(self, lineNumber):
        '''
        Returns a LogLine of the log at lineNumber
        @param lineNumber: line number to return
        '''

        return self.logLines[lineNumber]

    def open(self):
        '''
        Open the log file object.
        '''
        self.file = open(self.path, 'r')

    def reset(self):
        '''
        Reset the internal log structure to empty values.
        '''

        self.logLines = []

        self.errorLines = []
        self.warningLines = []
        self.debugLines = []
        self.infoLines = []

        self.currentErrorWarningLine = 0

    def getMtime(self):
        '''
        Returns the log's modification time in the format of seconds since the last epoch.
        '''

        return os.stat(self.path).st_mtime

    def parse(self):
        '''
        Parses a log and creates all LogLines. Also sets the error level of the current log.
        '''
        for i, line in enumerate(self.file.readlines()):
            ll = LogLine(line)
            if ll.level == 'ERROR':
                self.errorLines.append(i)
            elif ll.level == 'WARNING':
                self.warningLines.append(i)
            elif ll.level == "INFO":
                self.infoLines.append(i)
            elif ll.level == "DEBUG":
                self.debugLines.append(i)
            self.logLines.append(ll)

    def close(self):
        '''
        Close the log file object.
        '''
        self.file.close()

    def process(self):
        '''
        Processes a log. Does all the work, if the current modification time is
        newer than the one remembered in self.mtime.
        '''
        if self.getMtime() > self.mtime:
            self.reset()
            self.open()
            self.parse()
            self.close()
            self.mtime = self.getMtime()
            return True

        return False

    def icon(self):
        '''
        Return the icon associated with the current error level
        '''

        if len(self.errorLines):
            return appIcons['ERROR']
        elif len(self.warningLines):
            return appIcons['WARNING']
        else:
            return None

    def numLines(self):
        '''
        Returns the number of lines the log has
        '''
        return len(self.logLines)

    def __repr__(self):
        return "Log(%s)" % (self.path)
