#make sure we use all v2 APIs
import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)
sip.setapi('QTime', 2)
sip.setapi('QDate', 2)
sip.setapi('QDateTime', 2)

import sys

from PyQt4.QtGui import QApplication

#create app before we create the icons in the cache
app = QApplication(sys.argv)

from ui.resources.iconcache import appIcons

#setup app, do not change the app name, organization and domain
#since they are used to locate the preferences
app.setApplicationName("pkgPower")
app.setWindowIcon(appIcons['main'])
app.setOrganizationName("risefx")
app.setOrganizationDomain("risefx.com")

#finally create the main window and enter main event loop
from ui.mainwindow.MainWindow import MainWindow

mainWindow = MainWindow()
mainWindow.show()
sys.exit(app.exec_())
